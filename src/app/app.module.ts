import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { MaterialModule } from './material';
import { HeaderComponent } from './shared/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { RegionsComponent } from './regions/regions.component';
import { AddRegionComponent } from './regions/add-region/add-region.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ControlCenterComponent } from './control-center/control-center.component';

const appRoutes: Routes = [

  {
    path: 'add-region',
    component: AddRegionComponent,

  },
  {
    path: '',
    pathMatch: 'full',
    component: HeaderComponent,
  }

];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegionsComponent,
    AddRegionComponent,
    ControlCenterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
