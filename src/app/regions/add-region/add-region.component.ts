import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegionsService } from './region.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-region',
  templateUrl: './add-region.component.html',
  styleUrls: ['./add-region.component.css']
})
export class AddRegionComponent implements OnInit {
  regionForm: FormGroup;
  allRegions;
  allContries;
  constructor(private regionService: RegionsService, private fb: FormBuilder, private route: Router) { }

  ngOnInit() {
    this.regionForm = this.fb.group({
      rname: ['', [Validators.required, Validators.maxLength(100)]],
      country_id_fk: ['', Validators.required],
      address: ['', [Validators.required, Validators.maxLength(100)]],
      store_address: ['', [Validators.required, Validators.maxLength(100)]],
      contact_num: ['', [Validators.required, Validators.maxLength(13)]],
      fax_num: ['', [Validators.required, Validators.maxLength(100)]],
      maps_loc: ['', [Validators.required, Validators.maxLength(1000)]],
    });

    this.getAllContries();
  }

  getAllContries() {
    this.regionService.getCountries()
      .subscribe(data => {
        this.allContries = data['data'];
        console.log(data);
      });
  }

  saveRegion() {
    const hcode = JSON.parse(localStorage.getItem('currentUser')).hcode;
    this.regionService.saveRegion({ ...this.regionForm.value, user_hcode: hcode })

      .subscribe((data) => {
        console.log(data);

        console.log(data.status, data.message);
        if (data.status === "SUCCESS") {
          this.route.navigateByUrl("");
        }
        // this.getAllRegions();
        // const alertMessage = {
        //   status: data.status,
        //   message: data.message
        // };
        // this.alertMessgageService.alertMessage.next(alertMessage);
        this.regionForm.reset();
      });
  }

  // getAllRegions() {
  //   this.regionService.getRegions()
  //     .subscribe(data => {
  //       this.allRegions = data;
  //       console.log(data);
  //     });
  // }

}
