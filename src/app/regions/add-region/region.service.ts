// import { RestAPIConfig } from './../rest-api.config';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class RegionsService {
  constructor(private httpClient: HttpClient) { }

  saveRegion(regionDetails: any) {
    // console.log(regionDetails);
    return this.httpClient.post<{ status: string, message: string }>
      ('http://52.66.213.147:3000/api/userManagement/createRegion', regionDetails);
  }// saveRegion

  getRegions() {
    return this.httpClient.get<any>('http://52.66.213.147:3000/api/userManagement/getAllRegions');
  }// getRegions

  getCountries() {
    return this.httpClient.get<any>('http://52.66.213.147:3000/api/userManagement/getAllCountries');
  }// getCountries

  updateRegion(region_hcode, regionDetails) {
    // console.log(regionDetails);
    return this.httpClient.post<{ status: string, message: string }>('http://52.66.213.147:3000/api/userManagement/updateRegion/' + region_hcode, regionDetails);
  }

  getRegionById(r_hcode) {
    // console.log(r_hcode);
    return this.httpClient.get<{ status: string, message: string, data: any }>('http://52.66.213.147:3000/api/userManagement/getRegionById/' + r_hcode);
  }
}
