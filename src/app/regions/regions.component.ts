import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-regions',
  templateUrl: './regions.component.html',
  styleUrls: ['./regions.component.css']
})
export class RegionsComponent implements OnInit {

  constructor(private httpclient: HttpClient) { }


  displayedColumns: string[] = ['rname', 'cname', 'address', 'contact_num', 'actions'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {


    this.httpclient.get<any>('http://52.66.213.147:3000/api/userManagement/getAllRegions')
      .subscribe((res) => {
        if (!res) {
          return
        }
        this.dataSource = new MatTableDataSource(res.data);
        this.dataSource.paginator = this.paginator;
        console.log(res.data)
      })
  }
}



